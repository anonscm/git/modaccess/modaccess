/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.thema.data.GlobalDataStore;
import org.thema.common.DayTime;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.IOFeature;


/**
 *
 * @author gvuidel
 */
public class ODMatrixShpDialog extends javax.swing.JDialog {

    public boolean isOk = false;
    public Map<Object, DefaultFeature> pointOrig, pointDest;
    public boolean minDist;
    public long start;
    public int period;
    public byte day;
    public boolean reverse;
    public File pathFile, matrixFile, tableFile;

    /** Creates new form ODMatrixDialog */
    public ODMatrixShpDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(parent);
        getRootPane().setDefaultButton(okButton);

    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        destInfoLabel = new javax.swing.JLabel();
        startTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        dayComboBox = new javax.swing.JComboBox();
        startRadioButton = new javax.swing.JRadioButton();
        minRadioButton = new javax.swing.JRadioButton();
        periodSpinner = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        pathSelectFilePanel = new org.thema.common.swing.SelectFilePanel();
        matrixSelectFilePanel = new org.thema.common.swing.SelectFilePanel();
        tableSelectFilePanel = new org.thema.common.swing.SelectFilePanel();
        departRadioButton = new javax.swing.JRadioButton();
        endRadioButton = new javax.swing.JRadioButton();
        origVectorLayerPanel = new org.thema.data.ui.SelectVectorLayerPanel();
        destVectorLayerPanel = new org.thema.data.ui.SelectVectorLayerPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/modaccess/Bundle"); // NOI18N
        setTitle(bundle.getString("ODMatrixShpDialog.title")); // NOI18N

        okButton.setText(bundle.getString("ODMatrixShpDialog.okButton.text")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(bundle.getString("ODMatrixShpDialog.cancelButton.text")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        startTextField.setText(bundle.getString("ODMatrixShpDialog.startTextField.text")); // NOI18N

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, startRadioButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), startTextField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jLabel2.setText(bundle.getString("ODMatrixShpDialog.jLabel2.text")); // NOI18N

        dayComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));

        buttonGroup1.add(startRadioButton);
        startRadioButton.setSelected(true);
        startRadioButton.setText(bundle.getString("ODMatrixShpDialog.startRadioButton.text")); // NOI18N

        buttonGroup1.add(minRadioButton);
        minRadioButton.setText(bundle.getString("ODMatrixShpDialog.minRadioButton.text")); // NOI18N

        periodSpinner.setModel(new javax.swing.SpinnerNumberModel(60, 1, null, 1));

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, minRadioButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), periodSpinner, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jLabel1.setText(bundle.getString("ODMatrixShpDialog.jLabel1.text")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, minRadioButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), jLabel1, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("ODMatrixShpDialog.jPanel1.border.title"))); // NOI18N

        pathSelectFilePanel.setDescription(bundle.getString("ODMatrixShpDialog.pathSelectFilePanel.description")); // NOI18N
        pathSelectFilePanel.setFileDesc(bundle.getString("ODMatrixShpDialog.pathSelectFilePanel.fileDesc")); // NOI18N
        pathSelectFilePanel.setFileExts(bundle.getString("ODMatrixShpDialog.pathSelectFilePanel.fileExts")); // NOI18N
        pathSelectFilePanel.setSaveMode(true);

        matrixSelectFilePanel.setDescription(bundle.getString("ODMatrixShpDialog.matrixSelectFilePanel.description")); // NOI18N
        matrixSelectFilePanel.setFileDesc(bundle.getString("ODMatrixShpDialog.matrixSelectFilePanel.fileDesc")); // NOI18N
        matrixSelectFilePanel.setFileExts(bundle.getString("ODMatrixShpDialog.matrixSelectFilePanel.fileExts")); // NOI18N
        matrixSelectFilePanel.setSaveMode(true);

        tableSelectFilePanel.setDescription(bundle.getString("ODMatrixShpDialog.tableSelectFilePanel.description")); // NOI18N
        tableSelectFilePanel.setFileDesc(bundle.getString("ODMatrixShpDialog.tableSelectFilePanel.fileDesc")); // NOI18N
        tableSelectFilePanel.setFileExts(bundle.getString("ODMatrixShpDialog.tableSelectFilePanel.fileExts")); // NOI18N
        tableSelectFilePanel.setSaveMode(true);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(pathSelectFilePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, matrixSelectFilePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(tableSelectFilePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(pathSelectFilePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(matrixSelectFilePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(tableSelectFilePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonGroup2.add(departRadioButton);
        departRadioButton.setSelected(true);
        departRadioButton.setText(bundle.getString("ODMatrixShpDialog.departRadioButton.text")); // NOI18N

        buttonGroup2.add(endRadioButton);
        endRadioButton.setText(bundle.getString("ODMatrixShpDialog.endRadioButton.text")); // NOI18N

        origVectorLayerPanel.setDescription(bundle.getString("ODMatrixShpDialog.origVectorLayerPanel.description")); // NOI18N

        destVectorLayerPanel.setDescription(bundle.getString("ODMatrixShpDialog.destVectorLayerPanel.description")); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(destVectorLayerPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(layout.createSequentialGroup()
                        .add(origVectorLayerPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 593, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(destInfoLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(okButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cancelButton))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(12, 12, 12)
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(periodSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 61, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(startRadioButton)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(startTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jLabel2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(dayComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(51, 51, 51)
                                .add(departRadioButton))
                            .add(layout.createSequentialGroup()
                                .add(minRadioButton)
                                .add(254, 254, 254)
                                .add(endRadioButton)))
                        .add(0, 0, Short.MAX_VALUE))))
        );

        layout.linkSize(new java.awt.Component[] {cancelButton, okButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(87, 87, 87)
                        .add(destInfoLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(origVectorLayerPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(destVectorLayerPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(startTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(startRadioButton)
                    .add(jLabel2)
                    .add(dayComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(departRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(minRadioButton)
                    .add(endRadioButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(periodSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cancelButton)
                    .add(okButton)))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed

        try {
            List<DefaultFeature> origins = IOFeature.loadFeatures(origVectorLayerPanel.getSelectedFile(), origVectorLayerPanel.getIdField());
            List<DefaultFeature> dests = IOFeature.loadFeatures(destVectorLayerPanel.getSelectedFile(), destVectorLayerPanel.getIdField());
            pointOrig = new HashMap<>();
            for(DefaultFeature f : origins) {
                pointOrig.put(f.getId(), f);
            }
            pointDest = new HashMap<>();
            for(DefaultFeature f : dests) {
                pointDest.put(f.getId(), f);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ODMatrixShpDialog.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "An error occured :\n" + ex.getLocalizedMessage());
            return;
        }

        minDist = minRadioButton.isSelected();
        if(minDist) {
            period = (Integer)periodSpinner.getValue();
        } else {
            start = DayTime.string2Long(startTextField.getText());
        }
        day = (byte) (1 << dayComboBox.getSelectedIndex());

        pathFile = pathSelectFilePanel.getSelectedFile();
        matrixFile = matrixSelectFilePanel.getSelectedFile();
        tableFile = tableSelectFilePanel.getSelectedFile();
        reverse = endRadioButton.isSelected();
        
        isOk = true;
        setVisible(false);
        dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox dayComboBox;
    private javax.swing.JRadioButton departRadioButton;
    private javax.swing.JLabel destInfoLabel;
    private org.thema.data.ui.SelectVectorLayerPanel destVectorLayerPanel;
    private javax.swing.JRadioButton endRadioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private org.thema.common.swing.SelectFilePanel matrixSelectFilePanel;
    private javax.swing.JRadioButton minRadioButton;
    private javax.swing.JButton okButton;
    private org.thema.data.ui.SelectVectorLayerPanel origVectorLayerPanel;
    private org.thema.common.swing.SelectFilePanel pathSelectFilePanel;
    private javax.swing.JSpinner periodSpinner;
    private javax.swing.JRadioButton startRadioButton;
    private javax.swing.JTextField startTextField;
    private org.thema.common.swing.SelectFilePanel tableSelectFilePanel;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

}
