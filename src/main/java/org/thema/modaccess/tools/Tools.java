/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess.tools;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.linearref.LengthIndexedLine;
import org.locationtech.jts.operation.linemerge.LineMerger;
import org.locationtech.jts.precision.GeometryPrecisionReducer;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ProgressMonitor;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Node;
import org.thema.common.DayTime;
import org.thema.common.collection.HashMapList;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.Feature;
import org.thema.graph.SpatialGraph;
import org.thema.graph.Util;
import org.thema.network.GraphNetworkFactory;
import org.thema.network.Network;
import org.thema.network.Network.Mode;
import org.thema.network.NetworkLocation;
import org.thema.network.TimeTableNetwork;
import org.thema.network.WeightNetwork;
import org.thema.network.data.EdgeProperties;
import org.thema.network.data.Line;
import org.thema.network.data.NodeProperties;
import org.thema.network.data.Stop;
import org.thema.network.data.Train;
import org.thema.network.dijkstra.DefaultWeighter;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder.NetworkPath;
import org.thema.network.dijkstra.DijkstraNode;

/**
 *
 * @author gvuidel
 */
public class Tools {


    public static void calcSinuosity(List<DefaultFeature> features, GeometryPrecisionReducer reducer) {
        DefaultFeature.addAttribute("Sinus", features, -1.0);
        SpatialGraph sgraph = new SpatialGraph(features,reducer);
        Graph graph = sgraph.getGraph();

        for(Object n : graph.getNodes()) {
            Node node = (Node) n;
            if(node.isVisited() || node.getDegree() != 2) {
                continue;
            }
            HashSet<Edge> edges = new HashSet<>();
            edges.addAll(node.getEdges());
            node.setVisited(true);
            Iterator it = node.getRelated();
            Node n1 = (Node) it.next();
            // si il n'y a pas de second noeud c'est une boucle
            // on ne fait rien
            if(!it.hasNext()) {
                continue;
            }
            Node n2 = (Node) it.next();

            while(n1 != n2 && (n1.getDegree() == 2 || n2.getDegree() == 2)) {
                if(n1.getDegree() == 2) {
                    for (Object e : n1.getEdges()) {
                        if(!((Edge)e).getOtherNode(n1).isVisited()){
                            n1.setVisited(true);
                            n1 = ((Edge)e).getOtherNode(n1);
                            edges.add((Edge)e);
                            break;
                        }
                    }
                }
                if(n2.getDegree() == 2) {
                    for (Object e : n2.getEdges()) {
                        if(!((Edge)e).getOtherNode(n2).isVisited()){
                            n2.setVisited(true);
                            n2 = ((Edge)e).getOtherNode(n2);
                            edges.add((Edge)e);
                            break;
                        }
                    }
                }
            }

            LineMerger merger = new LineMerger();
            for(Edge e : edges) {
                merger.add(reducer.reduce(Util.getGeometry(e)));
            }
            Collection lines = merger.getMergedLineStrings();
            if(lines.size() > 1) {
                throw new RuntimeException("LineString not merge !!");
            }
            LineString line = (LineString) lines.iterator().next();
            double sin = line.getCoordinateN(0).distance(line.getCoordinateN(line.getNumPoints()-1))
                    / line.getLength();

            for(Edge e : edges) {
                ((DefaultFeature)e.getObject()).setAttribute("Sinus", sin);
            }

        }

        for(DefaultFeature f : features) {
            if(((Number)f.getAttribute("Sinus")).doubleValue() == -1) {
                LineString line = Util.geomToLineString(f.getGeometry());
                double sin = line.getCoordinateN(0).distance(line.getCoordinateN(line.getNumPoints()-1))
                        / line.getLength();
                f.setAttribute("Sinus", sin);
            }
        }
    }
    
    public static List<DefaultFeature> mergeLines(List<DefaultFeature> features, GeometryPrecisionReducer reducer) {

        SpatialGraph sgraph = new SpatialGraph(features,reducer);
        Graph graph = sgraph.getGraph();
        HashSet<Edge> restEdges = new HashSet<>(graph.getEdges());
        List<DefaultFeature> mergeLines = new ArrayList<>();
         
        for(Object n : graph.getNodes()) {
            Node node = (Node) n;
            if(node.isVisited() || node.getDegree() != 2) {
                continue;
            }
            HashSet<Edge> edges = new HashSet<>();
            edges.addAll(node.getEdges());
            node.setVisited(true);
            Iterator it = node.getRelated();
            Node n1 = (Node) it.next();
            // si il n'y a pas de second noeud c'est une boucle
            // on ne fait rien
            if(!it.hasNext()) {
                continue;
            }
            Node n2 = (Node) it.next();

            while(n1 != n2 && (n1.getDegree() == 2 || n2.getDegree() == 2)) {
                if(n1.getDegree() == 2) {
                    for (Object e : n1.getEdges()) {
                        if(!((Edge)e).getOtherNode(n1).isVisited()){
                            n1.setVisited(true);
                            n1 = ((Edge)e).getOtherNode(n1);
                            edges.add((Edge)e);
                            break;
                        }
                    }
                }
                if(n2.getDegree() == 2) {
                    for (Object e : n2.getEdges()) {
                        if(!((Edge)e).getOtherNode(n2).isVisited()){
                            n2.setVisited(true);
                            n2 = ((Edge)e).getOtherNode(n2);
                            edges.add((Edge)e);
                            break;
                        }
                    }
                }
            }

            LineMerger merger = new LineMerger();
            for(Edge e : edges) {
                merger.add(reducer.reduce(Util.getGeometry(e)));
            }
            Collection lines = merger.getMergedLineStrings();
            if(lines.size() > 1) {
                throw new RuntimeException("LineString not merge !!");
            }
            LineString line = (LineString) lines.iterator().next();
            DefaultFeature mergeLine = new DefaultFeature((DefaultFeature)edges.iterator().next().getObject(), true);
            mergeLine.setGeometry(line);
            mergeLines.add(mergeLine);
            restEdges.removeAll(edges);

        }
        
        for(Edge e : restEdges) {
            mergeLines.add((DefaultFeature)e.getObject());
        }
        
        return mergeLines;
    }

    public static List<DefaultFeature> splitLineAtIntersect(List<DefaultFeature> features) {
        DefaultFeatureCoverage<DefaultFeature> cov = new DefaultFeatureCoverage<>(features);
        List<DefaultFeature> results = new ArrayList<>(features.size());
        
        int i = 1;
        for(Feature f : features) {
            List<DefaultFeature> prox = cov.getFeatures(f.getGeometry().getEnvelopeInternal());
            List<Geometry> inters = new ArrayList<>();
            for(Feature fp : prox) {
                if(f != fp) {
                    Geometry g = f.getGeometry().intersection(fp.getGeometry());
                    inters.add(g);
                }
            }
            LengthIndexedLine lineRef = new LengthIndexedLine(f.getGeometry());
            TreeSet<Double> index = new TreeSet<>();
            for(Geometry inter : inters) {
                for (Coordinate c : inter.getCoordinates()) {
                    index.add(lineRef.project(c));
                }
            }

            index.add(0.0);
            index.add(f.getGeometry().getLength());
            Double prec = null;
            for(Double ind : index) {
                if(prec != null) {
                    DefaultFeature r = new DefaultFeature(i++, lineRef.extractLine(prec, ind), f.getAttributeNames(), f.getAttributes());
                    results.add(r);
                }
                prec = ind;

            }
        }

        return results;
    }

    public static List<Feature> checkAllPaths(Graph g, Network net) {
        ProgressMonitor monitor = new ProgressMonitor(null, "Check all paths", "", 0, g.getNodes().size());
        int i = 0;
        List<Feature> errLines = new ArrayList<>();
        for(Object n1 : g.getNodes()) {
            DijkstraNetworkPathFinder pathfinder = new DijkstraNetworkPathFinder(
                        net.getLocations((Feature)((Node)n1).getObject()),
                        new DefaultWeighter(), 0);
            pathfinder.calculate();
            monitor.setProgress(i++);
            Collection<DijkstraNode> calcNodes = pathfinder.getComputedNodes();
            if(calcNodes.size() == g.getNodes().size()) {
                continue;
            }
            HashSet<Node> nodes = new HashSet<>(g.getNodes());
            for(DijkstraNode n : calcNodes) {
                nodes.remove(n.getNode());
            }
            for(Node n : nodes) {
                Geometry line = new GeometryFactory().createLineString(new Coordinate[] {Util.getGeometry((Node)n1).getCentroid().getCoordinate(),
                    Util.getGeometry(n).getCentroid().getCoordinate()});
                errLines.add(new DefaultFeature(n.getID(), line, null, null));
            }
            monitor.close();
            return errLines;
        }
        monitor.close();
        return errLines;
    }

    public static String checkTCNetwork(TimeTableNetwork net, Collection<? extends Feature> nodeFeatures,
            String nodeIdAttr, File hsqlFile) throws Exception {

        StringBuilder logMsg = new StringBuilder();
        int nbNode = 0;
        HashSet<String> arrets = new HashSet<>();

        for(Feature f : nodeFeatures) {
            Stop a = new Stop(f.getAttribute(nodeIdAttr).toString());
            if(arrets.contains(a.getId().toString())) {
                logMsg.append("L'arrêt " + a.getId() + " existe en double !\n");
            }
            arrets.add(a.getId().toString());
        }

        System.out.println("Number of nodes added : " + nbNode);

        Class.forName("org.hsqldb.jdbcDriver");
        String pathdb = hsqlFile.getAbsolutePath().replace(".script", "");
        Connection dbConn = DriverManager.getConnection("jdbc:hsqldb:file:" + pathdb, "sa", "");
        Statement st = dbConn.createStatement();

        ResultSet set = st.executeQuery("SELECT * FROM Arret;");
        while(set.next()) {
            String idArret = set.getObject(1).toString();
            String idLigne = set.getObject(3).toString();
            if(!arrets.contains(idArret)) {
                if(idArret.toString().endsWith("_" + idLigne)) {
                    String subId = idArret.toString().replace("_"+idLigne, "");
                    if(arrets.contains(subId)) {
                        nbNode++;
                        arrets.add(idArret);
                    }
                    //else
                    //    logMsg.append("L'arrêt " + subId + " n'existe pas dans les données géométriques !\n");

                }
                //else
                //    logMsg.append("L'arrêt " + idArret + " n'existe pas dans les données géométriques !\n");

            }
            if(!arrets.contains(set.getObject(1).toString())) {
                logMsg.append("L'arrêt " + set.getString(1) + " - ligne " + idLigne + " n'existe pas dans les données géométriques !\n");
            }
        }

        return logMsg.toString();
    }

    public static String affectBus(TimeTableNetwork net, Collection<DefaultFeature> lines) {
        DefaultFeature.addAttribute("Length", lines, 0);
        HashMapList<String, Feature> mapLine = new HashMapList<>();
        for(DefaultFeature l : lines) {
            l.setAttribute("Length", l.getGeometry().getLength());
            mapLine.putValue((String)l.getAttribute("id_ligne"), l);
        }

        HashMap<String, GraphNetworkFactory> mapNet = new HashMap<>();
        for(String line : mapLine.keySet()) {
            HashSet set = new HashSet();
            List<Feature> lst = new ArrayList<>();
            for(Feature f : mapLine.get(line)) {
                if(set.contains(f.getAttribute("ID"))) {
                    continue;
                }
                set.add(f.getAttribute("ID"));
                lst.add(f);
            }
            System.out.println("Add ligne : " + line);
            GraphNetworkFactory n = new GraphNetworkFactory();
            n.addWeightNetwork(new WeightNetwork("bus", Mode.MAP, 1),
                    lst, null, "ID", Arrays.asList("Length"));
            mapNet.put(line, n);
        }

        HashMapList<EdgeProperties, String> mapEdge = new HashMapList<>();
        HashMap<String, TreeMap<Long, Integer>> mapTroncon = new HashMap<>();
        TreeMap<Long, Integer> init = new TreeMap<>();
        for(int i = 0; i < 24; i++) {
            init.put(i*DayTime.HOUR, 0);
        }

        for(GraphNetworkFactory n : mapNet.values()) {
            for (EdgeProperties edge : n.getTotalNetwork().getEdges()) {
                mapTroncon.put(edge.getId().toString(), new TreeMap<>(init));
            }
        }

        for(EdgeProperties edge : net.getEdges()) {
            if(!(edge.getParam() instanceof Line)) {
                continue;
            }
            String line = (String) ((Line)edge.getParam()).getId();
            if(!mapNet.containsKey(line)) {
                continue;
            }
            Network n = mapNet.get(line).getTotalNetwork();
            DijkstraNetworkPathFinder pathfinder = new DijkstraNetworkPathFinder(
                    n.getLocations(edge.getNodeA()), new DefaultWeighter(), 0);
            pathfinder.calculate();
            NetworkLocation loc = pathfinder.getBestDestLoc(n.getLocations(edge.getNodeB()));
            if(loc == null) {
                System.err.println("No path found  : " + edge.getNodeA().getParam() + " - " + edge.getNodeB().getParam());
                continue;
            }
            NetworkPath path = pathfinder.getNetworkPath(loc);
            // si l'origine et la destination sont sur le même troncon on ne compte rien
//            if(path.getOrigin().getGraphElem() == path.getDestination().getGraphElem())
//                continue;
//
//            String id = ((EdgeProperties)loc.getGraphElem().getObject()).getId().toString();
//            mapEdge.put(edge, id);
            for(Edge e : path.getAllEdges()) {
                String id = ((EdgeProperties)e.getObject()).getId().toString();
                mapEdge.putValue(edge, id);
            }
            
        }
        
        for(Line ligne : net.getLignes()) {
            if (mapNet.containsKey(ligne.getId())) {
                System.out.println("Ligne : " + ligne.getId());
                for (Train rame : ligne.getTrains()) {
                    if (!rame.isRunning(DayTime.TUESDAY)) {
                        continue;
                    }
                    Stop pa = null;
                    String precId = null;
                    for (Long t : rame.getTimes()) {
                        Stop a = rame.getStop(t);
                        if(pa == null) {
                            pa = a;
                            continue;
                        }
                        NodeProperties n1 = net.getNode(pa.getId());
                        NodeProperties n2 = net.getNode(a.getId());
                        if (mapEdge.containsKey(n1.getEdge(n2))) {
                            List<String> ids = new ArrayList<>(mapEdge.get(n1.getEdge(n2)));
                            if (ids.get(0).equals(precId)) {
                                ids.remove(0);
                            }
                            for(String id : ids) {
                                Long key = mapTroncon.get(id).floorKey(t % DayTime.DAY);
                                mapTroncon.get(id).put(key, mapTroncon.get(id).get(key)+1);
                                // edge inverse
                                if(id.endsWith("_1")) {
                                    id = id.substring(0, id.length()-2);
                                    key = mapTroncon.get(id).floorKey(t % DayTime.DAY);
                                    mapTroncon.get(id).put(key, mapTroncon.get(id).get(key)+1);

                                }
                            }
                            if (!ids.isEmpty()) {
                                precId = ids.get(ids.size()-1);
                            }
                        }
                        pa = a;
                    }
                }
            }
        }


        StringBuilder res = new StringBuilder();
        res.append("ID");
        for(Long t : init.keySet()) {
            res.append("\t" + DayTime.long2ShortString(t));
        }
        for(String id : mapTroncon.keySet()) {
            if (!id.endsWith("_1")) {
                res.append("\n" + id);
                for (Integer nb : mapTroncon.get(id).values()) {
                    res.append("\t" + nb);
                }
            }
        }

        return res.toString();
    }
    
    
    public static List<DefaultFeature> createStationTC(Collection<? extends Feature> nodeFeatures,
            String nodeIdAttr, File hsqlFile) throws SQLException  {
        HashMap<String, Coordinate> arrets = new HashMap<>();
        Set<Coordinate> nodes = new HashSet<>();
        GeometryPrecisionReducer reducer = new GraphNetworkFactory().getReducer();
        List<DefaultFeature> nodeSplit = new ArrayList<>();
        
        for(Feature f : nodeFeatures) {
            Point p = (Point)reducer.reduce(f.getGeometry());
            Coordinate c = p.getCoordinate();
            String id = f.getAttribute(nodeIdAttr).toString();
            if(nodes.contains(c)) { 
                c = dupAndMoveNode(p.getCoordinate(), id, nodes, reducer);
            }
            
            nodes.add(c);
            arrets.put(id, c);
        }

        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("No HSQL driver", ex);
        }
        String pathdb = hsqlFile.getAbsolutePath().replace(".script", "");
        Connection dbConn = DriverManager.getConnection("jdbc:hsqldb:file:" + pathdb, "sa", "");
        Statement st = dbConn.createStatement();
        
        ResultSet set = st.executeQuery("SELECT * FROM Arret;");
        while(set.next()) {
            String idArret = set.getObject(1).toString();
            String idLigne = set.getObject(3).toString();
            if(!arrets.containsKey(idArret)) {
                if(idArret.endsWith("_" + idLigne)) {
                    String subId = idArret.replace("_"+idLigne, "");
                    if(arrets.containsKey(subId)) {
                        Coordinate c = arrets.get(subId);
                        c = dupAndMoveNode(c, idArret, nodes, reducer);
                        nodes.add(c);
                        arrets.put(idArret, c);
                    } else {
                        throw new IllegalStateException("L'arrêt " + subId + " n'existe pas dans les données géométriques !");
//                        Logger.getLogger(GraphNetworkFactory.class.getName()).log(Level.WARNING, "L'arrêt " + subId + " n'existe pas dans les données géométriques !") ;
                    }

                } else {
                    throw new IllegalStateException("L'arrêt " + idArret + " n'existe pas dans les données géométriques !");
//                    Logger.getLogger(GraphNetworkFactory.class.getName()).log(Level.WARNING, "L'arrêt " + idArret + " n'existe pas dans les données géométriques !") ;
                }
            }
            if(!arrets.containsKey(set.getObject(1).toString())) {
                throw new IllegalStateException("L'arrêt " + set.getString(1) + " n'existe pas dans les données géométriques !");
//                Logger.getLogger(GraphNetworkFactory.class.getName()).log(Level.WARNING, "L'arrêt " + set.getString(1) + " n'existe pas dans les données géométriques !") ;
            }
            
            List<String> attrs = Arrays.asList("Nom");
            nodeSplit.add(new DefaultFeature(idArret, new GeometryFactory().createPoint(arrets.get(idArret)), attrs, Arrays.asList(set.getObject(2).toString())));
        }
        
        return nodeSplit;
    }
    
    /**
     * create the node for Stop a with a point move randomly from p
     * @param p
     * @param a
     * @return
     */
    private static Coordinate dupAndMoveNode(Coordinate c, String id, Set<Coordinate> nodes, GeometryPrecisionReducer reducer) {
        Point point = new GeometryFactory().createPoint(c);
        while(nodes.contains(point.getCoordinate())) {
            Coordinate coord = new Coordinate(point.getCoordinate());
            coord.x += Math.random();
            coord.y += Math.random();
            point = new GeometryFactory().createPoint(coord);
            point = (Point) reducer.reduce(point);
        }

        Logger.getLogger(GraphNetworkFactory.class.getName()).log(Level.FINE,
            "Node at the same position already exists : " + id + " -> move\n");

        return point.getCoordinate();
    }
}
