/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.thema.common.DayTime;
import org.thema.common.ProgressBar;
import org.thema.data.feature.Feature;
import org.thema.network.Network;
import org.thema.network.dijkstra.DefaultWeighter;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder;
import org.thema.network.dijkstra.NetworkPathFinder;
import org.thema.network.dijkstra.ReverseDijkstraNetworkPathFinder;
import org.thema.parallel.AbstractParallelTask;

/**
 * A parallel task for calculating the complete matrix of duration between a set of origins and destinations
 * 
 * @author Gilles Vuidel
 */
public class ODMatrixTaskDB extends AbstractParallelTask<Void, HashMap<Integer, double[]>> {

    private final Network network;
    private final List<? extends Feature> origins, destinations;
    private final long startTime;
    private final boolean reverse;
    private String connectionUrl;
    private String table;
    

    /**
     * Create a new task for executing in parallel with {@link ParallelFExecutor}
     * @param monitor the progress monitor
     * @param network the network
     * @param origins the origins points (centroid is used if the geometry is not point)
     * @param destinations the destinations points (centroid is used if the geometry is not point)
     * @param startTime the starting time in millisecond
     * @param reverse if true reverse the calculation : startTime is the ending time, origins become destinations and destinations origins
     */
    public ODMatrixTaskDB(ProgressBar monitor, Network network,
            List<? extends Feature> origins, List<? extends Feature> destinations, 
            long startTime, boolean reverse, String connectionUrl, String table) {
        super(monitor);
        this.network = network;
        this.origins = origins;
        this.destinations = destinations;
        this.startTime = startTime;
        this.reverse = reverse;
        this.connectionUrl = connectionUrl;
        this.table = table;
        monitor.setMaximum(origins.size());
    }

    @Override
    public void init() {
        super.init(); 
        try {
            if(connectionUrl.contains("postgresql")) {
                Class.forName("org.postgresql.Driver");
            } else {
                Class.forName("org.sqlite.JDBC");
            }
            try (Connection con = DriverManager.getConnection(connectionUrl)) {
                try(Statement stmt = con.createStatement()) {
                    stmt.execute("DROP TABLE IF EXISTS " + table + ";");
                    stmt.execute("CREATE TABLE " + table + " (orig text, dest text, distance real);");
                } 
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ODMatrixTaskDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public HashMap<Integer, double[]> execute(int start, int end) {
        HashMap<Integer, double[]> results = new HashMap();

        for(int i = start; i < end; i++) {
            if(isCanceled()) {
                return null;
            }
            NetworkPathFinder pathfinder;
            if(reverse) {
                pathfinder = new ReverseDijkstraNetworkPathFinder(
                    network.getLocations(origins.get(i)), new DefaultWeighter(), startTime);
            } else {
                pathfinder = new DijkstraNetworkPathFinder(
                    network.getLocations(origins.get(i)), new DefaultWeighter(), startTime);
            }
            pathfinder.calculate();
            int j = 0;
            double [] vector = new double[destinations.size()];
            for(Feature dest : destinations) {
               Double cost = pathfinder.getBestCost(network.getLocations(dest));
               vector[j] = cost == null ? Double.NaN : cost;
               j++;
            }
            results.put(i, vector);
            incProgress(1);
        }

        return results;
    }

    @Override
    public int getSplitRange() {
        return origins.size();
    }

    /**
     * The first dimension is for origins and the second is for destinations
     * @return nothing, results are sent to db
     */
    @Override
    public Void getResult() {
        return null;
    }

    
    public void gatherPG(HashMap<Integer, double[]> results) {
        PipedOutputStream pipedOut = null;
        try {
            PipedInputStream pipedIn = new PipedInputStream((int) (Runtime.getRuntime().maxMemory() / 100));
            pipedOut = new PipedOutputStream(pipedIn);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try (Connection con = DriverManager.getConnection(connectionUrl)) {
                        CopyManager copyManager = new CopyManager((BaseConnection) con);
                        copyManager.copyIn("COPY " + table + " FROM STDIN WITH DELIMITER '|'", pipedIn);
                    } catch (SQLException | IOException ex) {
                        Logger.getLogger(ODMatrixTaskDB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }).start();
            
            try (OutputStreamWriter writer = new OutputStreamWriter(pipedOut)) {
                for (int indO : results.keySet()) {
                    String idO = origins.get(indO).getId().toString();
                    double[] v = results.get(indO);
                    for (int i = 0; i < v.length; i++) {
                        writer.write(idO + "|" + destinations.get(i).getId().toString() + "|");
                        if (v[i] == Double.MAX_VALUE) {
                            writer.write("null");
                        } else {
                            writer.write(Double.toString(v[i] / DayTime.MINUTE));
                        }
                        writer.write("\n");
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(ODMatrixTaskDB.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                pipedOut.close();
            } catch (IOException ex) {
                Logger.getLogger(ODMatrixTaskDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void gather(HashMap<Integer, double[]> results) {
        if(connectionUrl.contains("postgresql")) {
            gatherPG(results);
            return;
        }
        try(Connection con = DriverManager.getConnection(connectionUrl)) {
            con.setAutoCommit(false);
            try (PreparedStatement stmt = con.prepareStatement("INSERT INTO " + table + " VALUES (?,?,?)")) {
                for (int indO : results.keySet()) {
                    String idO = origins.get(indO).getId().toString();
                    double[] v = results.get(indO);
                    for (int i = 0; i < v.length; i++) {
                        stmt.setString(1, idO);
                        stmt.setString(2, destinations.get(i).getId().toString());
                        if (v[i] == Double.MAX_VALUE) {
                            stmt.setNull(3, Types.REAL);
                        } else {
                            stmt.setDouble(3, v[i] / DayTime.MINUTE);
                        }
                        stmt.addBatch();
                    }
                    stmt.executeBatch();
                }
                stmt.executeBatch();
                con.commit();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ODMatrixTaskDB.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

}
