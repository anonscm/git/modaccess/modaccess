/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.modaccess;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import org.geotools.graph.structure.Edge;
import org.geotools.graph.structure.Graph;
import org.thema.common.Config;
import org.thema.common.ConsoleProgress;
import org.thema.common.DayTime;
import org.thema.common.ProgressBar;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.Feature;
import org.thema.graph.SpatialGraph;
import org.thema.network.GraphNetworkFactory;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.ParallelExecutor;

/**
 *
 * @author gvuidel
 */
public class CLI {
    private static Project project;
    
    public static void execute(String[] argArr) throws Exception {
        if(argArr[0].equals("--help")) {    
            System.out.println("Usage :\njava -jar modaccess.jar [-proc n]\n"  +
                    "--mainpart network_layer_file\n" +
                    "--project project_file.xml COMMAND\n\n" +
                    "Command list :\n" +
                    "--odmatrix origins=layer [ido=fieldname] destinations=layer [idd=fieldname] [start=hh:mm] [reverse] [output=name]\n");
            return;
        }
        
        List<String> args = new ArrayList<>(Arrays.asList(argArr));
        
        // parallel options
        while(!args.isEmpty() && !args.get(0).startsWith("--")) {
            String p = args.remove(0);
            switch (p) {
                case "-proc":
                    int n = Integer.parseInt(args.remove(0));
                    ParallelFExecutor.setNbProc(n);
                    ParallelExecutor.setNbProc(n);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown option " + p);
            }
        }
        if(args.isEmpty()) {
            throw new IllegalArgumentException("Need --project");
        }
        
        Config.setProgressBar(new ConsoleProgress());
        
        String c = args.remove(0);
        if(c.equals("--mainpart")) {
            partitions(new File(args.remove(0)));
            return;
        }
        
        File f = new File(args.remove(0));
        project = Project.load(f);
        
        while(!args.isEmpty()) {
            String arg = args.remove(0);
            switch(arg) {
                case "--odmatrix":
                    odMatrix(args);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown command : " + arg);
            }
        }
    }

    private static void odMatrix(List<String> args) throws IOException, ClassNotFoundException, SQLException {
        Map<String, String> params = extractAndCheckParams(args, Arrays.asList("origins", "destinations"), 
                Arrays.asList("ido", "idd", "start", "reverse", "output"));
        List<DefaultFeature> origins;
        String origin = params.get("origins");
        if(origin.toLowerCase().endsWith(".shp") || origin.toLowerCase().endsWith(".gpkg")) {
            origins = IOFeature.loadFeatures(new File(origin), params.get("ido"));
        } else {
            origins = IOFeature.loadFeatures(project.getDatastore().getFeatureSource(origin), params.get("ido"));
        }                 
        
        List<DefaultFeature> destinations;
        String destination = params.get("destinations");
        if(destination.toLowerCase().endsWith(".shp") || destination.toLowerCase().endsWith(".gpkg")) {
            destinations = IOFeature.loadFeatures(new File(destination), params.get("ido"));
        } else {
            destinations = IOFeature.loadFeatures(project.getDatastore().getFeatureSource(destination), params.get("ido"));
        }     
        
        long start = 0;
        if(params.containsKey("start")) {
            start = DayTime.string2Long(params.get("start"));
        }
        
        boolean reverse = params.containsKey("reverse");
        
        String output = project.getDatastore() == null ? "od.db" : "od";
        if(params.containsKey("output")) {
            output = params.get("output");
        }
        String url, table;
        if(output.toLowerCase().endsWith(".db") || output.toLowerCase().endsWith(".sqlite")) {
            url = "jdbc:sqlite:" + output;
            table = "od";
        } else {
            url = project.getConnectionUrl();
            table = output;
        }
        
        ProgressBar monitor = Config.getProgressBar("OD Matrix");
        
        ODMatrixTaskDB task = new ODMatrixTaskDB(monitor, project.getTotalNetwork(), origins, destinations, start, reverse, url, table);
        
        ExecutorService.execute(task);
      
    }
    
    
    private static Map<String, String> extractAndCheckParams(List<String> args, List<String> mandatoryParams, List<String> optionalParams) {
        Map<String, String> params = new LinkedHashMap<>();
                
        while(!args.isEmpty() && !args.get(0).startsWith("--")) {
            String arg = args.remove(0);
            if(arg.contains("=")) {
                String[] tok = arg.split("=");
                params.put(tok[0], tok[1]);
            } else {
                params.put(arg, null);
            }
        }
        
        // check mandatory parameters
        if(!params.keySet().containsAll(mandatoryParams)) {
            HashSet<String> set = new HashSet<>(mandatoryParams);
            set.removeAll(params.keySet());
            throw new IllegalArgumentException("Mandatory parameters are missing : " + Arrays.deepToString(set.toArray()));
        }
        
        // check unknown parameters if optionalParams is set
        if(optionalParams != null) {
            HashSet<String> set = new HashSet<>(params.keySet());
            set.removeAll(mandatoryParams);
            set.removeAll(optionalParams);
            if(!set.isEmpty()) {
                throw new IllegalArgumentException("Unknown parameters : " + Arrays.deepToString(set.toArray()));
            }
        }
        
        return params;
    }

    public static void partitions(File f) throws IOException {
        SpatialGraph sgraph = new SpatialGraph(IOFeature.loadFeatures(f), 
                        new Project().getGraphFactory().getReducer());
        Graph graph = sgraph.getGraph();
        List<Graph> parts = GraphNetworkFactory.partition(graph);

        Graph largest = parts.get(0);
        for(Graph part : parts) {
            if(part.getNodes().size() > largest.getNodes().size()) {
                largest = part;
            }
        }
        HashSet<Feature> features = new HashSet<>();
        for(Object o : largest.getEdges()) {
            features.add((Feature)((Edge)o).getObject());
        }
        IOFeature.saveFeatures(features, new File(f.getParentFile(), f.getName().substring(0, f.getName().lastIndexOf(".")) + "-part.gpkg"),
                IOFeature.getCRS(f));
                
    }
}
