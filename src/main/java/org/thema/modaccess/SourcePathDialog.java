/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.List;
import org.thema.common.DayTime;
import org.thema.network.data.NodeProperties;
import org.thema.drawshape.PanelMap.ShapeMouseListener;
import org.thema.drawshape.SelectableShape;
import org.thema.data.feature.DefaultFeature;
import org.thema.drawshape.layer.DefaultGroupLayer;
import org.thema.drawshape.layer.Layer;
import org.thema.network.NetworkLocation;
import org.thema.network.dijkstra.DefaultWeighter;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder;
import org.thema.network.dijkstra.NetworkPathFinder;
import org.thema.network.dijkstra.ReverseDijkstraNetworkPathFinder;
import org.thema.network.shape.SourceGraphLayer;


/**
 *
 * @author Gilles Vuidel
 */
public class SourcePathDialog extends javax.swing.JDialog {

    private long start;
    private byte day;
    private String nodeId;
    private Coordinate point;
    private MainFrame mainFrm;
    private Project project;
    
    private NetworkPathFinder pathfinder;
    private SourceGraphLayer layer;

    /** Creates new form SourcePathDialog */
    public SourcePathDialog(MainFrame mainFrm) {
        super(mainFrm, false);
        initComponents();
        setLocationRelativeTo(mainFrm);
        getRootPane().setDefaultButton(okButton);

        this.mainFrm = mainFrm;
        project = mainFrm.getProject();
        mainFrm.getMapViewer().addMouseListener(new ShapeMouseListener() {

            @Override
            public void mouseClicked(Point2D p, List<SelectableShape> shapes, MouseEvent sourceEvent, int cursorMode) {
                pointTextField.setText(p.getX() + ", " + p.getY());
            }
        });
    }

    private NetworkPathFinder getPathFinder() {
        if(pathfinder != null) {
            return pathfinder;
        }

        if(nodeRadioButton.isSelected()) {
            nodeId = nodeTextField.getText();
        } else {
            String [] tok = pointTextField.getText().split(",");
            point = new Coordinate(Double.parseDouble(tok[0].trim()), Double.parseDouble(tok[1].trim()));
        }
        String [] tok = startTextField.getText().split(":");
        day = (byte) (1 << dayComboBox.getSelectedIndex());
        start = (Integer.parseInt(tok[0]) * 60 + Integer.parseInt(tok[1])) * 60 * 1000;


        NodeProperties node = null;
        if(pointRadioButton.isSelected()) {
            List<NetworkLocation> locs = project.getTotalNetwork().getLocations(new DefaultFeature("P", new GeometryFactory().createPoint(point), null, null));
            if(startRadioButton.isSelected()) {
                pathfinder = new DijkstraNetworkPathFinder(locs,
                        new DefaultWeighter(), DayTime.getTotalTime(start, day));
            } else {
                pathfinder = new ReverseDijkstraNetworkPathFinder(locs,
                        new DefaultWeighter(), DayTime.getTotalTime(start, day));
            }
        } else {

//            for(NodeProperties n : mainFrm.graphFactory.getTotalNetwork().getNodes())
//                if(n.getParam() != null && n.getParam() instanceof Arret)
//                    if(((Arret)n.getParam()).id.toString().equals(nodeId)) {
//                        node = n;
//                        break;
//                    }
//
//            pathfinder = new DijkstraNetworkPathFinder(mainFrm.graphFactory.getGraph(), node.getNode(),
//                    mainFrm.graphFactory.getTotalNetwork(), DayTime.getTotalTime(start, day));
        }

        return pathfinder;
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        nodeTextField = new javax.swing.JTextField();
        startTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        dayComboBox = new javax.swing.JComboBox();
        nodeRadioButton = new javax.swing.JRadioButton();
        pointRadioButton = new javax.swing.JRadioButton();
        pointTextField = new javax.swing.JTextField();
        debugButton = new javax.swing.JButton();
        startRadioButton = new javax.swing.JRadioButton();
        endRadioButton = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/thema/modaccess/Bundle"); // NOI18N
        setTitle(bundle.getString("SourcePathDialog.title")); // NOI18N
        setAlwaysOnTop(true);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, nodeRadioButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), nodeTextField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        startTextField.setText(bundle.getString("SourcePathDialog.startTextField.text")); // NOI18N

        jLabel2.setText(bundle.getString("SourcePathDialog.jLabel2.text")); // NOI18N

        okButton.setText(bundle.getString("SourcePathDialog.okButton.text")); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(bundle.getString("SourcePathDialog.cancelButton.text")); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jLabel3.setText(bundle.getString("SourcePathDialog.jLabel3.text")); // NOI18N

        dayComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }));

        buttonGroup.add(nodeRadioButton);
        nodeRadioButton.setText(bundle.getString("SourcePathDialog.nodeRadioButton.text")); // NOI18N
        nodeRadioButton.setEnabled(false);

        buttonGroup.add(pointRadioButton);
        pointRadioButton.setSelected(true);
        pointRadioButton.setText(bundle.getString("SourcePathDialog.pointRadioButton.text")); // NOI18N

        pointTextField.setText(bundle.getString("SourcePathDialog.pointTextField.text")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, pointRadioButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), pointTextField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        debugButton.setText(bundle.getString("SourcePathDialog.debugButton.text")); // NOI18N
        debugButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                debugButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(startRadioButton);
        startRadioButton.setSelected(true);
        startRadioButton.setText(bundle.getString("SourcePathDialog.startRadioButton.text")); // NOI18N

        buttonGroup1.add(endRadioButton);
        endRadioButton.setText(bundle.getString("SourcePathDialog.endRadioButton.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nodeRadioButton)
                            .addComponent(pointRadioButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pointTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                            .addComponent(nodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(debugButton)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(startRadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(endRadioButton)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(163, Short.MAX_VALUE)
                    .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cancelButton)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nodeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nodeRadioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pointRadioButton)
                    .addComponent(pointTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startRadioButton)
                    .addComponent(endRadioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(startTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(dayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(debugButton)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(164, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cancelButton)
                        .addComponent(okButton))
                    .addGap(18, 18, 18)))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        
        setVisible(false);
        dispose();

        getPathFinder().calculate();
        Layer l = new SourceGraphLayer("Source ", project.getGraph(), pathfinder,
                    new SourceGraphLayer.CostFormater() {
                        @Override
                        public String format(Double cost) {
                            if(cost == null || cost.isNaN()) {
                                return "N";
                            } else {
                                return DayTime.long2FullString(start + (startRadioButton.isSelected() ? 1 : -1) * cost.longValue());
                            }
                        }
                    });
        l.setRemovable(true);
        ((DefaultGroupLayer)mainFrm.getMapViewer().getLayers()).addLayerFirst(l);

}//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        setVisible(false);
        dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

    private void debugButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_debugButtonActionPerformed
        getPathFinder().calculateOneStep();

        if(layer == null) {
            layer = new SourceGraphLayer("Source ", project.getGraph(), pathfinder,
                    new SourceGraphLayer.CostFormater() {
                        @Override
                        public String format(Double cost) {
                            if(cost == null || cost.isNaN()) {
                                return "N";
                            } else {
                                return DayTime.long2FullString(start + (startRadioButton.isSelected() ? 1 : -1) * cost.longValue());
                            }
                        }
                    });
            ((DefaultGroupLayer)mainFrm.getMapViewer().getLayers()).addLayerFirst(layer);
        }


         layer.update(pathfinder);
    }//GEN-LAST:event_debugButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox dayComboBox;
    private javax.swing.JButton debugButton;
    private javax.swing.JRadioButton endRadioButton;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JRadioButton nodeRadioButton;
    private javax.swing.JTextField nodeTextField;
    private javax.swing.JButton okButton;
    private javax.swing.JRadioButton pointRadioButton;
    private javax.swing.JTextField pointTextField;
    private javax.swing.JRadioButton startRadioButton;
    private javax.swing.JTextField startTextField;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

}
