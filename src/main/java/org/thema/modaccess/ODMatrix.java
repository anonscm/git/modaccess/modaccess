/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.CancellationException;
import org.geotools.graph.structure.Graph;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.data.feature.Feature;
import org.thema.network.MultiNetwork;
import org.thema.network.NetworkLocation;
import org.thema.common.DayTime;
import org.thema.common.ProgressBar;
import org.thema.common.collection.TreeMapList;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.IOFeature;
import org.thema.network.dijkstra.DefaultWeighter;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder;
import org.thema.network.dijkstra.DijkstraNetworkPathFinder.NetworkPath;
import org.thema.network.dijkstra.ODMatrixTask;

/**
 *
 * @author gib
 */
public class ODMatrix {
    
    MultiNetwork networks;
    Graph graph;
    TreeMap<Object, Feature> origins;
    TreeMap<Object, Feature> destinations;
    
    long [][] startTime;
    double [][] minCost;

    public ODMatrix(MultiNetwork networks, Graph graph, TreeMap<Object, Feature> origins, TreeMap<Object, Feature> destinations) {
        this.networks = networks;
        this.graph = graph;
        this.origins = origins;
        this.destinations = destinations;
    }

     public void perform(boolean minDist, long start, byte day, int period, boolean reverse, ProgressBar monitor) throws IOException {

        startTime = minDist ? new long[origins.size()][destinations.size()] : null;
        minCost = new double[origins.size()][destinations.size()];
        for (double[] minCost1 : minCost) {
            Arrays.fill(minCost1, Double.MAX_VALUE);
        }

        long startCalc = System.currentTimeMillis();

        long time = minDist ? 0 : start;
        monitor.setMaximum((int)(minDist ? DayTime.DAY / (period*DayTime.MINUTE) : 1) * 100);
        int monInd = 0;
        while(minDist && time < DayTime.DAY || monInd == 0) {
            updateMatrix(origins.values(), destinations.values(),
                    DayTime.getTotalTime(time, day), reverse, monitor.getSubProgress(100));

            time += period * DayTime.MINUTE;
            monInd++;
        }

        System.out.println("Duration : " + (System.currentTimeMillis()-startCalc) / 1000);

    }


     public void saveMatrix(Appendable writer, String separator) throws IOException {

        writer.append("Orig\\Dest");
        for(Object destId : destinations.keySet()) {
            writer.append(separator + destId);
        }

        int i = 0;
        for(Object idO : origins.keySet()) {
            writer.append("\n" + idO);
            for(int j = 0; j < destinations.size(); j++) {
                if (minCost[i][j] == Double.MAX_VALUE) {
                    writer.append(separator + "NaN");
                } else {
                    writer.append(separator + Math.round(minCost[i][j] / DayTime.MINUTE));
                }
            }

            i++;
        }


        if(startTime != null) {
            writer.append("\n\nStart time\nOrig\\Dest");
            for(Object destId : destinations.keySet()) {
                writer.append(separator + destId);
            }

            i = 0;
            for(Object idO : origins.keySet()) {
                writer.append("\n" + idO);
                for(int j = 0; j < destinations.size(); j++) {
                    if (minCost[i][j] == Double.MAX_VALUE) {
                        writer.append(separator + "NaN");
                    } else {
                        writer.append(separator + DayTime.long2String(startTime[i][j]));
                    }
                }

                i++;
            }
        }
     }
     
     public void saveTable(File file) throws IOException, ClassNotFoundException, SQLException {
        
        if(!file.getName().toLowerCase().endsWith(".csv")) {
                Class.forName("org.sqlite.JDBC");
            try(Connection con = DriverManager.getConnection("jdbc:sqlite:" + file)) {
                try(Statement stmt = con.createStatement()) {
                    stmt.execute("DROP TABLE IF EXISTS od;");
                    stmt.execute(startTime != null ?
                        "CREATE TABLE od (orig text, dest text, distance real, start_time text);" : 
                        "CREATE TABLE od (orig text, dest text, distance real, rank_orig integer);");
                }
                con.setAutoCommit(false);
                try(PreparedStatement stmt = con.prepareStatement("INSERT INTO od VALUES (?,?,?,?)")) {
                    if(startTime != null) {
                        int i = 0;
                        for(Object idO : origins.keySet()) {
                            int j = 0;
                            for(Object idD : destinations.keySet()) {
                                stmt.setString(1, idO.toString());
                                stmt.setString(2, idD.toString());
                                if(minCost[i][j] == Double.MAX_VALUE) {
                                    stmt.setNull(3, Types.REAL);
                                    stmt.setNull(4, Types.VARCHAR);
                                } else {
                                    stmt.setDouble(3, minCost[i][j] / DayTime.MINUTE);
                                    stmt.setString(4, DayTime.long2String(startTime[i][j]));
                                }
                                stmt.execute();
                                j++;
                            }
                            i++;
                        }
                    } else {
                        int j = 0;
                        for(Object idD : destinations.keySet()) {
                            TreeMapList<Double, Object> idOrigins = new TreeMapList<>();
                            int i = 0;
                            for(Object idO : origins.keySet()) {
                                idOrigins.putValue(minCost[i][j], idO);
                                i++;
                            }
                            i = 1;
                            for(Double cost : idOrigins.keySet()) {
                                for(Object idO : idOrigins.get(cost)) {
                                    stmt.setString(1, idO.toString());
                                    stmt.setString(2, idD.toString());
                                    if(cost == Double.MAX_VALUE) {
                                        stmt.setNull(3, Types.REAL);
                                        stmt.setNull(4, Types.INTEGER);
                                    } else {
                                        stmt.setDouble(3, cost / DayTime.MINUTE);
                                        stmt.setInt(4, i);
                                    }
                                    stmt.execute();
                                    i++;
                                }
                            }
                            j++;
                        }
                    }
                    con.commit();
                }

            }
        } else {
            try (FileWriter writer = new FileWriter(file)) {
                if(startTime != null) {
                    writer.write("Orig,Dest,Distance,Start time\n");
                    int i = 0;
                    for(Object idO : origins.keySet()) {
                        int j = 0;
                        for(Object idD : destinations.keySet()) {
                            writer.write(idO + "," + idD + ",");
                            if(minCost[i][j] == Double.MAX_VALUE) {
                                writer.append(",");
                            } else {
                                writer.write(""+minCost[i][j] / DayTime.MINUTE);
                                writer.write("," + DayTime.long2String(startTime[i][j]));
                            }
                            writer.write("\n");
                            j++;
                        }
                        i++;
                    }
                } else {
                    writer.write("Orig,Dest,Distance,RankOrig\n");
                    int j = 0;
                    for(Object idD : destinations.keySet()) {
                        TreeMapList<Double, Object> idOrigins = new TreeMapList<>();
                        int i = 0;
                        for(Object idO : origins.keySet()) {
                            idOrigins.putValue(minCost[i][j], idO);
                            i++;
                        }
                        i = 1;
                        for(Double cost : idOrigins.keySet()) {
                            for(Object idO : idOrigins.get(cost)) {
                                writer.write(idO + "," + idD + ",");
                                if(cost == Double.MAX_VALUE) {
                                    writer.append(",");
                                } else {
                                    writer.write(""+ (cost / DayTime.MINUTE) + "," + i);
                                }
                                writer.write("\n");
                                i++;
                            }
                        }
                        j++;
                    }
                }
            }
        }
     }

     
     /**
      * Attention pour l'instant ne tient pas compte de l'heure de départ !!!!!
      * 
      * @param pathFile
      * @param monitor
      * @throws IOException
      */

    public void savePaths(File pathFile, ProgressBar monitor) throws IOException  {

        monitor.setNote("Saving path...");
        List<String> attrNames = Arrays.asList("IdOrig", "IdDest", "Distance");
        List<DefaultFeature> features = new ArrayList<>();
        int i = 0;
        for(Object idOrig : origins.keySet()) {
            monitor.setProgress(100*i/origins.size());
            if(monitor.isCanceled()) {
                throw new CancellationException();
            }
            DijkstraNetworkPathFinder pathfinder = new DijkstraNetworkPathFinder(
                    networks.getLocations(origins.get(idOrig)), 
                    new DefaultWeighter(), 0); // attention changer 0 en starttime
            pathfinder.calculate();
            int j = 0;
            for(Object idDest : destinations.keySet()) {
                NetworkLocation destLoc = pathfinder.getBestDestLoc(networks.getLocations(destinations.get(idDest)));
                NetworkPath path = pathfinder.getNetworkPath(destLoc);
                List attrs = Arrays.asList(idOrig, idDest, minCost[i][j]);
                features.add(new DefaultFeature(idDest, path.getPathGeom(), attrNames, attrs));
                j++;
            }
            i++;
        }

        IOFeature.saveFeatures(features, pathFile);

    }

    private void updateMatrix(Collection<Feature> origins, Collection<Feature> destinations, long start, boolean reverse, ProgressBar monitor) {
        ODMatrixTask task = new ODMatrixTask(monitor, networks, new ArrayList<>(origins),
                new ArrayList<>(destinations), start, reverse);
        new ParallelFExecutor(task).executeAndWait();
        double [][] cost = task.getResult();
        for(int i = 0; i < origins.size(); i++) {
            for(int j = 0; j < destinations.size(); j++) {
                if(cost[i][j] < minCost[i][j]) {
                    minCost[i][j] = cost[i][j];
                    if(startTime != null) {
                        startTime[i][j] = start;
                    }
                }
            }
        }
    }
}
