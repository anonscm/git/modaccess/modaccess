/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.thema.network.data.Stop;
import org.thema.common.DayTime;
import org.thema.network.data.Line;
import org.thema.network.data.Train;

/**
 * Version pour les fiches horaires par arrêt (SNCF)
 * 
 * 
 * @author gvuidel
 */
public class FusionCSVStation {

    /**
     * L'heure de changement de jour défaut 4h du matin
     */
    public static final long DAY_THRESHOLD = 4 * DayTime.HOUR;
    public static final int IND_HEURE = 0;
    public static final int IND_PROV = 1;
    public static final int IND_DEST = 2;
    public static final int IND_TRAIN = 3;


    public static String importHoraires(File dir) throws IOException, SQLException {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FusionCSVStation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        StringBuilder logMsg = new StringBuilder();

        Connection con = DriverManager.getConnection("jdbc:hsqldb:file:" + dir.getAbsolutePath(), "sa", "");

        Statement st = con.createStatement();
        st.execute("CREATE TABLE Ligne (id varchar, nom VARCHAR, CONSTRAINT ligne_pkey PRIMARY KEY (id));");
        st.execute("CREATE TABLE Arret (id_ar varchar, nom_ar varchar, id_ligne varchar, CONSTRAINT arret_pkey PRIMARY KEY (id_ar));");
        st.execute("CREATE TABLE Rame (id_rame integer, jours varchar, id_ligne varchar, CONSTRAINT bus_pkey PRIMARY KEY (id_rame));");
        st.execute("CREATE TABLE Trajet (id_ar varchar, id_rame integer, h_long integer, heure time, CONSTRAINT trajet_pkey PRIMARY KEY (id_rame, id_ar, h_long));");


        HashMap<String, Train> rames = new HashMap<String, Train>();
        HashMap<String, Line> lignes = new HashMap<String, Line>();
        HashMap<String, Stop> arrets = new HashMap<String, Stop>();

        HashMap<String, Long> startRame = new HashMap<String, Long>();

        for(File fd : dir.listFiles())
            if(fd.getName().endsWith(".csv")) {
                System.out.println(fd);
                logMsg.append("\n---- " + fd.getAbsolutePath());
                String [] tokens = fd.getName().split("_");
                if(tokens[1].equals("A.csv"))
                    continue;
                String idStation = tokens[0];
                File fa = new File(fd.getParentFile(), idStation + "_A.csv");


                if(idStation.equals("AGEN"))
                    System.out.println("Agen");

                CSVReader reader = new CSVReader(new FileReader(fd), ',', '"');
                String [] header = reader.readNext();
                String [] fields;
                HashMap<String, Long> trains = new HashMap<String, Long>();

                while ((fields = reader.readNext()) != null) {
                    String rame = fields[IND_TRAIN];
                    String ligne = fields[IND_PROV] + "-" + fields[IND_DEST];
                    if(!lignes.containsKey(ligne)) {
                        st.execute(String.format("INSERT INTO Ligne VALUES ('%s', '%s');", ligne, ligne));
                        lignes.put(ligne, new Line(ligne, ligne));
                    }

                    if(!rames.containsKey(rame)) {
                        rames.put(rame, new Train(Integer.parseInt(rame), lignes.get(ligne), "lmmjvsdf"));
                        st.execute(String.format("INSERT INTO Rame VALUES (%d, '%s', '%s');",
                            Integer.parseInt(rame), "lmmjvsdf", ligne));
                    }

                    if(trains.containsKey(rame)) {
                        //System.out.println("Train " + rame + " en doublon");
                        continue;
                    }

                    long time = DayTime.string2Long(fields[IND_HEURE].replace("h", ":"));
                    trains.put(rame, time);
                    String idArret = idStation + "_" + ligne;
                    if(!arrets.containsKey(idArret)) {
                        st.execute(String.format("INSERT INTO Arret VALUES ('%s', '%s', '%s');",
                            idArret, idStation, ligne));
                        arrets.put(idArret, new Stop(idArret));
                    }

                    rames.get(rame).addStop(time, arrets.get(idArret));

//                    st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
//                                    idArret, Integer.parseInt(rame), time, DayTime.long2String(time)));
                }

                reader.close();

                HashMap<String, Long> trainsA = new HashMap<String, Long>();
                reader = new CSVReader(new FileReader(fa), ',', '"');
                header = reader.readNext();
                while ((fields = reader.readNext()) != null) {
                    String rame = fields[IND_TRAIN];
                    String ligne = fields[IND_PROV] + "-" + fields[IND_DEST];
                    if(!lignes.containsKey(ligne)) {
                        st.execute(String.format("INSERT INTO Ligne VALUES ('%s', '%s');", ligne, ligne));
                        lignes.put(ligne, new Line(ligne, ligne));
                    }

                    if(!rames.containsKey(rame)) {
                        rames.put(rame, new Train(Integer.parseInt(rame), lignes.get(ligne), "lmmjvsdf"));
                        st.execute(String.format("INSERT INTO Rame VALUES (%d, '%s', '%s');",
                            Integer.parseInt(rame), "lmmjvsdf", ligne));
                    } else if(!rames.get(rame).getLine().getId().equals(ligne)) {
                        System.err.println("Train " + rame + " est sur 2 lignes : " + ligne + " et " + rames.get(rame).getLine().getId());
                    }

                    long time = DayTime.string2Long(fields[IND_HEURE].replace("h", ":"));

                    trainsA.put(rame, time);
                    String idArret = idStation + "_" + ligne;
                    if(trains.containsKey(rame)) {

                    } else {
                        trains.put(rame, time);

                        if(!arrets.containsKey(idArret)) {
                            st.execute(String.format("INSERT INTO Arret VALUES ('%s', '%s', '%s');",
                                idArret, idStation, ligne));
                            arrets.put(idArret, new Stop(idArret));
                        }

                        rames.get(rame).addStop(time, arrets.get(idArret));

//                        st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
//                                        idArret, Integer.parseInt(rame), time, DayTime.long2String(time)));
                    }
                }

                reader.close();

                // on sélectionne les rames qui sont seulement au départ
                HashSet<String> rameD = new HashSet<String>(trains.keySet());
                rameD.removeAll(trainsA.keySet());
                for(String rame : rameD)
                    startRame.put(rame, trains.get(rame));

            }

        if(!startRame.keySet().equals(rames.keySet()))
            System.out.println("pas le même nombre de rames");

        for(Train rame : rames.values()) {
            Long start = startRame.get(rame.getId().toString());
            if(start == null) {
                start = 0L;
                System.out.println("Pas de départ : " + rame.getId() + " " + rame.getLine().getId());
                long prec = rame.getTimes().iterator().next();
                for(long time : rame.getTimes()) {
                    if(time-prec > 12 * DayTime.HOUR) {
                        start = time;
                        System.out.println("Départ : " + DayTime.long2String(time));
                    }
                    prec = time;
                }
                
            }
            for(long time : rame.getTimes()) {
                Stop arret = rame.getStop(time);
                if(time < start)
                    time += DayTime.DAY;
                st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
                                        arret.getId(), rame.getId(), time, DayTime.long2String(time)));
            }
        }



//        for(File fd : dir.listFiles())
//            if(fd.getName().endsWith(".csv")) {
//                System.out.println(fd);
//                logMsg.append("\n---- " + fd.getAbsolutePath());
//                String [] tokens = fd.getName().split("_");
//                if(tokens[1].equals("A.csv"))
//                    continue;
//                String idStation = tokens[0];
//                File fa = new File(fd.getParentFile(), idStation + "_A.csv");
//
//
//                CSVReader reader = new CSVReader(new FileReader(fd), ',', '"');
//                String [] header = reader.readNext();
//                String [] fields;
//                HashMap<String, Long> trains = new HashMap<String, Long>();
//
//                while ((fields = reader.readNext()) != null) {
//                    String rame = fields[IND_TRAIN];
//                    String line = fields[IND_PROV] + "-" + fields[IND_DEST];
//                    if(!lignes.contains(line)) {
//                        st.execute(String.format("INSERT INTO Line VALUES ('%s', '%s');", line, line));
//                        lignes.add(line);
//                    }
//
//                    long time = DayTime.string2Long(fields[IND_HEURE].replace("h", ":"));
//                    if(!rames.containsKey(rame)) {
//                        rames.put(rame, null);
//                        st.execute(String.format("INSERT INTO Train VALUES (%d, '%s', '%s');",
//                            Integer.parseInt(rame), "lmmjvsdf", line));
//                    }
//
//                    if(trains.containsKey(rame)) {
//                        System.out.println("Train " + rame + " en doublon");
//                        continue;
//                    }
//                    trains.put(rame, time);
//                    String idArret = idStation + "_" + line;
//                    try {
//                    st.execute(String.format("INSERT INTO Stop VALUES ('%s', '%s', '%s');",
//                            idArret, idStation, line));
//                    } catch(Exception e) {
//                        System.out.println("Stop existant");
//                    }
//                    st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
//                                    idArret, Integer.parseInt(rame), time, DayTime.long2String(time)));
//                }
//
//                reader.close();
//
//                HashMap<String, Long> trainsA = new HashMap<String, Long>();
//                reader = new CSVReader(new FileReader(fa), ',', '"');
//                header = reader.readNext();
//                while ((fields = reader.readNext()) != null) {
//                    String rame = fields[IND_TRAIN];
//                    String line = fields[IND_PROV] + "-" + fields[IND_DEST];
//                    if(!rames.containsKey(rame)) {
//                        rames.put(rame, null);
//                        st.execute(String.format("INSERT INTO Train VALUES (%d, '%s', '%s');",
//                            Integer.parseInt(rame), "lmmjvsdf", line));
//                        if(!lignes.contains(line)) {
//                            st.execute(String.format("INSERT INTO Line VALUES ('%s', '%s');", line, line));
//                            lignes.add(line);
//                        }
//                    }
//
//                    long time = DayTime.string2Long(fields[IND_HEURE].replace("h", ":"));
//                    trainsA.put(rame, time);
//
//                    String idArret = idStation + "_" + line;
//                    if(trains.containsKey(rame)) {
//
//                    } else {
//                        trains.put(rame, time);
//                        try {
//                        st.execute(String.format("INSERT INTO Stop VALUES ('%s', '%s', '%s');",
//                                idArret, idStation, line));
//                        } catch(Exception e) {
//                        System.out.println("Stop existant");
//                        }
//                        st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
//                                        idArret, Integer.parseInt(rame), time, DayTime.long2String(time)));
//                    }
//                }
//
//                reader.close();
//
//                // on sélectionne les rames qui sont seulement au départ
//                HashSet<String> rameD = new HashSet<String>(trains.keySet());
//                rameD.removeAll(trainsA.keySet());
//                for(String rame : rameD)
//                    rames.put(rame, trains.get(rame));
//
//            }


        st.execute("COMMIT;");
        st.execute("SHUTDOWN;");
        st.close();
        con.commit();
        con.close();

        return logMsg.toString();
    }

}
