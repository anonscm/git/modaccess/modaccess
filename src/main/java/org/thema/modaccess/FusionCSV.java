/*
 * Copyright (C) 2017 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.modaccess;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.thema.common.DayTime;

/**
 *
 * @author gvuidel
 */
public class FusionCSV {

    /**
     * L'heure de changement de jour défaut 4h du matin
     */
    public static final long DAY_THRESHOLD = 3 * DayTime.HOUR;
    public static final int NB_COL_HEADER = 2;
    public static final int IND_ID_ARRET = 0;
    public static final int IND_NOM_ARRET = 1;
    public static final int IND_NOM_LIGNE = -1;

    public static String importHoraires(File dir, String name) throws IOException, SQLException {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("No HSQL driver", ex);
        }
        
        StringBuilder logMsg = new StringBuilder();

        try (Connection con = DriverManager.getConnection("jdbc:hsqldb:file:" + new File(dir, name), "sa", "")) {
            Statement st = con.createStatement();
            st.execute("CREATE TABLE Ligne (id varchar(20), nom varchar(50), CONSTRAINT ligne_pkey PRIMARY KEY (id));");
            st.execute("CREATE TABLE Arret (id_ar varchar(20), nom_ar varchar(50), id_ligne varchar(20), CONSTRAINT arret_pkey PRIMARY KEY (id_ar));");
            st.execute("CREATE TABLE Rame (id_rame integer, jours varchar(10), id_ligne varchar(20), CONSTRAINT bus_pkey PRIMARY KEY (id_rame));");
            st.execute("CREATE TABLE Trajet (id_ar varchar(20), id_rame integer, h_long integer, heure time, CONSTRAINT trajet_pkey PRIMARY KEY (id_rame, id_ar, h_long));");
            //        HashSet<String> arrets = new HashSet<String>();
            int idBus = 1;
            for (File f : dir.listFiles()) {
                if (f.getName().endsWith(".csv")) {
                    System.out.println(f);
                    logMsg.append("\n---- " + f.getAbsolutePath());
                    String [] tokens = f.getName().split("_");
                    String idLigne = tokens[0];
                    if (tokens.length >= 2) {
                        idLigne += "_" + tokens[1];
                    }
                    try (CSVReader reader = new CSVReader(new FileReader(f), ',', '"')) {
                        String [] header = reader.readNext();
                        int dIdBus = 0;
                        for (int i = NB_COL_HEADER; i < header.length; i++) {
                            if (header[i].startsWith("x")) {
                                String days = normalizeDays(header[i-1].trim());
                                int n = Integer.parseInt(header[i].trim().substring(1));
                                for (int j = 0; j < n; j++) {
                                    st.execute(String.format("INSERT INTO Rame VALUES (%d, '%s', '%s');",
                                            idBus + dIdBus + i + j - NB_COL_HEADER, days, idLigne));
                                }
                                dIdBus += n-1;
                            } else {
                                if (header[i].trim().isEmpty()) {
                                    logMsg.append("\nErreur jour de circulation vide - " + f.getName() + " - colonne " + (i+1));
                                }
                                try {
                                    String days = normalizeDays(header[i].trim());
                                    st.execute(String.format("INSERT INTO Rame VALUES (%d, '%s', '%s');",
                                            idBus + dIdBus + i-NB_COL_HEADER, days, idLigne));
                                } catch(Exception e) {
                                    logMsg.append("\nErreur jour de circulation (" + header[i] + ") - " + f.getName() + " - colonne " + (i+1));
                                }   }
                        }
                        String [] fields;
                        String nomLigne = IND_NOM_LIGNE != -1 ? header[IND_NOM_LIGNE].replace("'", "''") : "";
                        st.execute(String.format("INSERT INTO Ligne VALUES ('%s', '%s');", idLigne, nomLigne));
                        List<List<Long>> heures = new ArrayList<>();
                        List<String> stations = new ArrayList<>();
                        for (int i = NB_COL_HEADER; i < header.length+dIdBus; i++) {
                            heures.add(new ArrayList<Long>());
                        }
                        while ((fields = reader.readNext()) != null) {
                            //                    if(nomLigne == null) {
//                        nomLigne = fields[1].replace("'", "''");
//                        st.execute(String.format("INSERT INTO Ligne VALUES ('%s', '%s');", idLigne, nomLigne));
//                    }
                            String idAr = fields[IND_ID_ARRET].replace("'", "''").trim();
                            String nomAr = IND_NOM_ARRET > -1 ? fields[IND_NOM_ARRET].replace("'", "''").trim() : idAr;
                            if (idAr.isEmpty()) {
                                continue;
                            }
                            idAr += "_" + idLigne;
                            if (!stations.contains(idAr)) {
                                st.execute(String.format("INSERT INTO Arret VALUES ('%s', '%s', '%s');", idAr, nomAr, idLigne));
//                        arrets.add(idAr);
                            } else {
                                //if(stations.contains(idAr))
                                System.out.println("Arret " + idAr + "duppliqué dans la même ligne");
                            }
                            //                    else {
//                        idAr += "_" + idLigne;
//                        if(stations.contains(idAr))
//                            System.out.println("Arret " + idAr + "duppliqué dans la même ligne");
//                        else {
//                            st.execute(String.format("INSERT INTO Arret VALUES ('%s', '%s', '%s');", idAr, nomAr, idLigne));
//                            arrets.add(idAr);
//                            System.out.println("Arret " + idAr + " créé car doublon avec une autre ligne");
//                        }   
//                    }
                    stations.add(idAr);
                            dIdBus = 0;
                            for (int i = NB_COL_HEADER; i < fields.length; i++) {
                                String heure = fields[i].trim();
                                if (heure.contains("x")) {
                                    String [] tok = heure.split("x");
                                    int min = Integer.parseInt(tok[0]);
                                    int n = Integer.parseInt(tok[1]);
                                    long t = heures.get(i-1+dIdBus-NB_COL_HEADER).get(heures.get(i+dIdBus-NB_COL_HEADER).size());
                                    for (int j = 0; j < n; j++) {
                                        if (t == Long.MAX_VALUE) {
                                            heures.get(i+j+dIdBus-NB_COL_HEADER).add(Long.MAX_VALUE);
                                        } else {
                                            heures.get(i+j+dIdBus-NB_COL_HEADER).add(t+(j+1)*min *DayTime.MINUTE);
                                        }
                                    }
                                    dIdBus += n-1;
                                } else {
                                    if (!heure.equals("_") && !heure.isEmpty() && !heure.equals("-") && !heure.equals("|")) {
                                        //st.execute(String.format("INSERT INTO Trajet VALUES (%d, %d, '%s');", idAr, idBus+i-4, heure));
                                        if (heure.equals("?")) {
                                            heures.get(i+dIdBus-NB_COL_HEADER).add(Long.MAX_VALUE);
                                        } else {
                                            try {
                                                heures.get(i+dIdBus-NB_COL_HEADER).add(string2Time(heure));
                                            } catch(Exception e) {
                                                logMsg.append("\nErreur format heure (" + heure + ") - " + f.getName() + " - Arrêt " + idAr + " - colonne " + (i+1));
                                            }       }
                                    } else {
                                        heures.get(i+dIdBus-NB_COL_HEADER).add(null);
                                    }
                                }
                            }
                        }
                        // on ajoute 24 heures aux horaires inférieur à DAY_THRESHOLD
                        for (List<Long> rame : heures) {
                            for (int i = 0; i < rame.size(); i++) {
                                Long t = rame.get(i);
                                if (t != null && t != Long.MAX_VALUE && t < DAY_THRESHOLD) {
                                    rame.set(i, rame.get(i) + 24 * DayTime.HOUR);
                                }
                            }
                        }
                        for (List<Long> rame : heures) {
                            ListIterator<Long> it = rame.listIterator();
                            Long lastTime = it.next();
                            while (it.hasNext()) {
                                int ind = it.nextIndex();
                                Long time = it.next();
                                if (time != null) {
                                    if (lastTime != null) {
                                        if(time < lastTime && DAY_THRESHOLD == 0) {
                                            time += 24 * DayTime.HOUR;
                                            rame.set(ind, time);
                                        }       if (time < lastTime) {
                                            logMsg.append("\nErreur : Horaire décroissant sur l'arrêt " + stations.get(ind) + " " + DayTime.long2String(time) + " - Ligne " + idLigne);
                                    time = null;
                                    rame.set(ind, null);
                                        } else if (time == (long)lastTime) {
                                            int nb = 2;
                                            Long nextTime = null;
                                            while (it.hasNext() && (nextTime == null || nextTime == (long)time)) {
                                                nextTime = it.next();
                                                if (nextTime != null && nextTime == (long)time) {
                                                    nb++;
                                                }
                                            }
                                            //System.out.println("Horaire duppliqué " + nb + " fois à l'arrêt " + stations.get(ind));
                                            while (it.previousIndex() >= ind) {
                                                it.previous();
                                            }
                                            for (int i = ind, n = 0; n < nb-1; i++) {
                                                if(rame.get(i) != null) {
                                                    rame.set(i, lastTime + (60000 / nb) * (n+1));
                                            n++;
                                                }   }
                                        } else if (time == Long.MAX_VALUE) {
                                            int nb = 2;
                                            Long nextTime = null;
                                            while (it.hasNext() && (nextTime == null || nextTime == Long.MAX_VALUE)) {
                                                nextTime = it.next();
                                                if (nextTime != null && nextTime == Long.MAX_VALUE) {
                                                    nb++;
                                                }
                                            }
                                            while (it.previousIndex() >= ind) {
                                                it.previous();
                                            }
                                            if (nextTime == null || nextTime == Long.MAX_VALUE) {
                                                logMsg.append("\nImpossible d'interpoler les horaires : pas d'horaire final -> station : " + stations.get(ind));
                                                time = null;
                                                rame.set(ind, null);
                                            } else {
                                                long d = nextTime-lastTime;
                                                if(d == 0) {
                                            nb++;
                                            d = 60000;
                                                }       //System.out.println("Horaire interpolé " + nb + " fois à l'arrêt " + stations.get(ind));
                                                for (int i = ind, n = 0; n < nb-1; i++) {
                                                    if(rame.get(i) != null) {
                                                rame.set(i, lastTime + (d / nb) * (n+1));
                                                n++;
                                            }   }
                                            }
                                        }
                                    }
                                }
                                if (time != null && time != Long.MAX_VALUE) {
                                    lastTime = time;
                                }
                            }
                        }
                        int indBus = 0;
                        for (List<Long> rame : heures) {
                            int indAr = 0;
                            for (Long time : rame) {
                                if (time != null && time != Long.MAX_VALUE) {
                                    st.execute(String.format("INSERT INTO Trajet VALUES ('%s', %d, %d, '%s');",
                                            stations.get(indAr), idBus+indBus, time, DayTime.long2String(DayTime.getTimeOfDay(time))));
                                }
                                indAr++;
                            }
                            indBus++;
                        }
                        idBus += indBus;//header.length-NB_COL_HEADER;
                    }
                }
            }
            st.execute("COMMIT;");
            st.execute("SHUTDOWN;");
            st.close();
            con.commit();
        }

        return logMsg.toString();
    }


    private static String concat(String s1, String s2) {
        char [] res = s1.toCharArray();
        for(int i = 0; i < 8; i++) {
            if (res[i] == '-') {
                res[i] = s2.charAt(i);
            } else if (s2.charAt(i) != '-' && s2.charAt(i) != res[i]) {
                throw new RuntimeException("Erreur de codage des jours !!!!");
            }
        }
        return new String(res);
    }
    
    private static String normalizeDays(String days) {
        //spécifique CG33
        if(days.indexOf("\ufffd") > -1) {
            days = days.replace("\ufffd", "");
        }

        days = days.toLowerCase();
        // format Besac  dlamjvs | s | d
        if(days.contains("a") || days.length() == 1) {
            HashMap<Character, String> map = new HashMap<>();
            map.put('l', "l-------");
            map.put('a', "-m------");
            map.put('m', "--m-----");
            map.put('j', "---j----");
            map.put('v', "----v---");
            map.put('s', "-----s--");
            map.put('d', "------d-");
            map.put('f', "-------f");
            String res = "--------";
            for(char c : days.toCharArray()) {
                res = concat(res, map.get(c));
            }
            days = res;
        } else { // format CG33 lmmjvsdf
            String d = "l00jvsdf";
            for(int j = 0; j < d.length(); j++) {
                int ind = days.indexOf(d.charAt(j));
                if(ind > -1 && ind != j) {
                    if (ind - j > 0) {
                        days = days.substring(ind-j);
                    } else {
                        days = days.substring(0, days.length()+(ind-j));
                    }
                }
            }
            while(days.length() < 8) {
                days += "-";
            }
            if(days.length() > 8) {
                days = days.substring(0, 8);
            }
        }
        return days;
    }

    public static long string2Time(String time) {
        String [] tok = time.trim().split("[:,\\. ]");
        long t = Integer.parseInt(tok[0]) * DayTime.HOUR;
        if(tok.length > 1) {
            if(tok[1].length() == 1) {
                tok[1] += "0";
            }
            t += Integer.parseInt(tok[1]) * DayTime.MINUTE;
            if(tok.length > 2) {
                t += Integer.parseInt(tok[2]) * DayTime.SECOND;
            }
        }
        return t;
    }
}
