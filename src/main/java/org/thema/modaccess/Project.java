/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.modaccess;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.data.DataStore;
import org.geotools.graph.structure.Graph;
import org.locationtech.jts.geom.PrecisionModel;
import org.thema.common.Util;
import org.thema.common.param.XMLObject;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;
import org.thema.network.AbstractNetwork;
import org.thema.network.GraphNetworkFactory;
import org.thema.network.GraphNetworkFactory.NetworkParam;
import org.thema.network.InterNetwork;
import org.thema.network.MultiNetwork;
import org.thema.network.Network;

/**
 *
 * @author gvuidel
 */
public class Project {

    private Map conParams;
    
    private List<NetworkParam> netParams;
    
    private transient DataStore datastore;
    private transient GraphNetworkFactory graphFactory;

    public Project() {
        try {
            setNetworks(new ArrayList<>());
        } catch (Exception ex) {
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public GraphNetworkFactory getGraphFactory() {
        return graphFactory;
    }

    public MultiNetwork getTotalNetwork() {
        return graphFactory.getTotalNetwork();
    }

    public void addNetwork(AbstractNetwork net, File layer, Object[] params) throws Exception {
        List<DefaultFeature> edges = layer != null ? IOFeature.loadFeatures(layer) : null;
        graphFactory.addNetwork(net, edges, params);
        netParams.add(new GraphNetworkFactory.NetworkParam(net, layer, params));
    }

    public Graph getGraph() {
        return graphFactory.getGraph();
    }

    public void saveProject(File f) throws IOException {
        try(FileWriter w = new FileWriter(f)) {
            Util.getXStream().toXML(this, w);
        } 
    }
    
    public void removeNetwork(String name) throws Exception {
        List<NetworkParam> networks = XMLObject.dupplicate(netParams);
        for(GraphNetworkFactory.NetworkParam net : new ArrayList<>(networks)) {
            if(net.network.getName().equals(name)) {
                networks.remove(net);
            }
        }
        setNetworks(networks);
    }

    private void setNetworks(List<NetworkParam> networks) throws Exception {
        graphFactory = new GraphNetworkFactory(new PrecisionModel(10), new InterNetwork("AutoIntercon", Network.Mode.MAP, 60));
        netParams = networks;
        graphFactory.loadNetworks(networks);
    }
    
    public DataStore getDatastore() throws IOException {
        if(datastore == null && conParams != null) {
            datastore = IOFeature.getDataStore(conParams);
        }
        return datastore;
    }
    
    public String getConnectionUrl() {
        if(conParams == null) {
            throw new IllegalStateException("No postgresql connection parameters in the project");
        }
        return "jdbc:postgresql://" + conParams.get("host") + ":" + conParams.get("port") + "/" + 
                conParams.get("database") +"?user=" + conParams.get("user") + "&password=" + conParams.get("passwd");
    }
    
    public static Project load(File f) throws Exception {
        Project p = (Project) Util.getXStream().fromXML(f);
        p.setNetworks(p.netParams);
        return p;
    }

}
