## Changelog

##### version 1.0beta5 (19/09/2022)
- Java 16 compatibility for timetable network

##### version 1.0beta4 (11/08/2022)
- CLI : add --mainpart command

##### version 1.0beta3 (28/01/2022)
- CLI : --odmatrix ido and idd parameters became optional

##### version 1.0beta2 (20/01/2022)
- Bug loading some shapefiles with Z coordinates with or without M values

##### version 1.0beta1 (15/10/2021)
- Manage GTFS CalendarDate

##### version 1.0beta (22/09/2021)
- Java 16 compatibility 
- OD matrix export in csv
- Network loading : add geopackage format

##### version 1.0alpha1 (02/02/2021)
WARNING project is not compatible with previous version

- add command line interface (CLI) with command --odmatrix
- add interconnection network support

##### version 1.0alpha (29/01/2021)
WARNING project is not compatible with previous version

- add Postgis support just in XML file, not in UI

##### version 0.9.9 (27/11/2020)
WARNING project is not compatible with previous version

- Geopackage is default vector format
- OD matrix table can be saved in sqlite database

##### version 0.9.8 (21/11/2019)
- Better project error management when saving and loading
- Network can be removed

##### version 0.9.7 (26/03/2019)
- Correct error "Cost decrease" when a bus nevers circulates (days=0) in some cases in gtfs

##### version 0.9.6 (26/06/2018)
- In Nearest ressource, add origins layer with nearest ressource id and distance

##### version 0.9.5 (27/03/2018)
- Java 9 support

##### version 0.9.4 (24/10/2017)
- Optimization of path calculation on timetable network

##### version 0.9.3 (03/10/2017)
- Add rank to ODMatrix table

##### version 0.9.2 (27/09/2017)
- Add GTFS support
- Add Nearest ressource feature
- Reverse calculations are correct even with negative times
- Add basic project feature

##### version 0.9.1 (16/03/2017)
- Merge menu Build time DB and Create station
- Source and Path layers can be removed
- Add AutoInterconnect network to interconnect PT net with Walk net automatically
- Few bug corrections and translations

##### version 0.9 (15/03/2017)
- Rename project to MODAccess